#!/usr/bin/env python
# vim: set ft=python et ts=4 sw=4:

import errno
import fnmatch
import glob
import gzip
import logging
import os
import re
import shutil
import sys

keyValEX   = re.compile(r'^([^: ]+): *(.*)$')
contValEx  = re.compile(r'^ (.*)$')
versionEx  = re.compile(r'^([^:]+): *(.*)$')
dependsEx  = re.compile(r'^ *([^ ]+)')
ipkVerEx   = re.compile(r'^([^:]+:)?([^:].*)$')

def path_join(*args):
    return os.path.normpath(os.path.join(*args))

class Package(object):
    def __init__(self, d):
        self.data = d

    def __str__(self):
        return self.Name

    def __repr__(self):
        return "Package(%s, %s, %s, ...)" % (self.Name, self.Version, self.Architecture)

    def __getitem__(self, key):
        return self.data.get(key)

    def __eq__(self, other):
        ret = False
        ill = False

        if isinstance(other, Package) and self.Name == other.Name:
            if self.Version == other.Version:
                ret = True

                for k in [ 'Architecture' ]:
                    if self[k] != other[k]:
                        logging.warning("%s: %s defers (%s vs %s)",
                                self.Name, k, self[k], other[k])
                        ill = True

                for k in [ 'Provides', 'Depends', 'Recommends' ]:
                    # ignore version restrictions
                    #
                    v0 = [ m.group(1) for m in [ dependsEx.match(v) for v in self[k]] if m] if self[k] else []
                    v1 = [ m.group(1) for m in [ dependsEx.match(v) for v in other[k]] if m] if other[k] else []

                    x  = sorted([ v for v in v0 if v not in v1 ])
                    v1 = sorted([ v for v in v1 if v not in v0 ])
                    v0 = x

                    if v0 or v1:
                        logging.warning("%s: %s defers (%r vs %r)",
                                self.Name, k, v0, v1)
                        ill = True

                if ill:
                    logging.warning("%s: ILL (%s)",
                            self.Name,
                            self.Version)

        return ret

    @property
    def Name(self):
        return self.data['Package']

    @property
    def Version(self):
        return self.data['Version']

    @property
    def Architecture(self):
        return self.data['Architecture']

    @property
    def Depends(self):
        deps = self.data.get('Depends', [])
        return [ m.group(1) for m in [ dependsEx.match(v) for v in deps] if m ]

    @property
    def Ipkfile(self):
        v = ipkVerEx.match(self.Version).group(2)
        f = "%s_%s_%s.ipk" % (self.Name, v, self.Architecture)
        return os.path.join(self.Architecture, f)

def parseManifest(filename):
    logging.info("Parsing %s", filename)

    k, v = None, None
    ret, d = {}, {}

    if fnmatch.fnmatch(filename, '*.gz'):
        f = gzip.GzipFile(filename, 'rt')
    else:
        f = open(filename, 'rt')

    for line in f:
        m = keyValEX.match(line)
        if m:
            k = m.group(1)
            if k == 'Conffiles':
                v = []
            elif k in [ 'Provides', 'Depends', 'Recommends' ]:
                v = sorted(m.group(2).split(', '))
            else:
                v = m.group(2).strip()

            d[k] = v
        elif line == '\n':
            if d:
                p, d = Package(d), {}
                ret[p.Name] = p
        elif k:
            m = contValEx.match(line)
            if m:
                v = m.group(1).strip()
                if k == 'Conffiles':
                    d[k].append(v.split(' ')[0])
                else:
                    d[k] = ' '.join([d[k], v])
            else:
                logging.warning("%r", line)
        else:
            logging.warning("%r", line)

    if d:
        p = Package(d)
        ret[p.Name] = p

    f.close()
    return ret

def DiscoverManifest(ref):
    m, basedir, ipkdir = None, None, None

    if os.path.isfile(ref):
        dirname, filename = os.path.split(ref)

        if filename == 'opkg_status.txt' or \
           fnmatch.fnmatch(filename, "SW-*-opkg_status.txt"):
            # manifest from release dir

            m = Manifest(ref)
            basedir = dirname
            ipkdir  = os.path.join(dirname, 'ipk')
        elif filename == 'versions.txt' or \
             fnmatch.fnmatch(filename, "SW-*-versions.txt"):
            # versions from release dir

            basedir = dirname
            ipkdir = os.path.join(dirname, 'ipk')

        elif filename in [ 'Packages', 'Packages.gz' ]:
            # IPKDIR/Packages.gz
            # IPKDIR/*/Packages.gz
            if os.path.isfile(path_join(dirname, '..', 'Packages.gz')):
                ipkdir = path_join(dirname, '..')
            else:
                ipkdir = dirname

        elif filename == "opkg.status":
            m = Manifest(ref)
            if os.path.isfile(os.path.join(dirname, '..', 'Packages.gz')):
                # IPKDIR/MACHINE/opkg.status
                ipkdir = path_join(dirname, '..')

        else:
            m = Manifest(ref)

    elif os.path.isdir(ref):
        # IPKDIR/Packages.gz
        # IPKDIR/*/Packages.gz
        if os.path.isfile(path_join(ref, '..', 'Packages.gz')):
            ipkdir = path_join(dirname, '..')
        elif os.path.isfile(path_join(ref, 'Packages.gz')):
            ipkdir = ref
        elif os.path.isfile(path_join(ref, 'ipk', 'Packages.gz')):
            ipkdir = path_join(ref, 'ipk')

        for p in [ 'opkg_status.txt', 'SW-*-opkg_status.txt' ]:
            x = glob.glob(path_join(ref, p))
            if len(x) > 0:
                m = Manifest(x[0])
                basedir = ref
                break

        if not basedir:
            for p in [ 'versions.txt', 'SW-*-versions.txt',
                       'hanover.version', 'hanover-*-version' ]:
                x = glob.glob(path_join(ref, p))
                if len(x) > 0:
                    basedir = ref
                    break

    if ipkdir:
        if not m:
            x = glob.glob(path_join(ipkdir, '*', 'opkg.status'))
            if len(x) > 0:
                m = Manifest(x[0])

        if not basedir:
            # ipk/../versions.txt
            for p in [ 'versions.txt', 'SW-*-versions.txt',
                       'hanover.version', 'hanover-*-version' ]:
                x = glob.glob(path_join(ipkdir, '..', p))
                if len(x) > 0:
                    basedir = os.path.dirname(x[0])

        if not basedir:
            # ipk/../images/MACHINE/scripts/
            p = path_join(ipkdir, '..', 'images', '*', 'scripts', '..')
            x = glob.glob(p)
            if len(x) > 0 and os.path.isdir(x[0]):
                basedir = x[0]

    if basedir and not m:
        for p in [ 'opkg_status.txt', 'SW-*-opkg_status.txt', 'opkg.status' ]:
            x = glob.glob(path_join(basedir, p))
            if len(x) > 0:
                m = Manifest(x[0])
                break

    if m:
        m.basedir = basedir
        m.ipkdir  = ipkdir
    else:
        m = Manifest(None, basedir=basedir, ipkdir=ipkdir)

    # attempt to load versions file
    if basedir:
        for p in [ 'versions.txt', 'SW-*-versions.txt',
                   'hanover.version', 'hanover-*-version' ]:
            x = glob.glob(path_join(basedir, p))
            if len(x) > 0:
                m.loadVersions(x[0])
                break

    return m

class PackageIterator:
    def __init__(self, d):
        self.l = [ (k, p) for k, p in d.iteritems() ]
        self.l.sort(key = lambda t: t[0])
        self.i, self.max = 0, len(self.l)

    def __next__(self):
        if self.i < self.max:
            t = self.l[self.i]
            self.i += 1

            return t[0], t[1]
        else:
            raise StopIteration()

    def next(self):
        return self.__next__()

class Manifest(object):

    def __init__(self, statusFile=None, ipkdir=None, basedir=None, versionFile=None):
        if statusFile:
            self.status = parseManifest(statusFile)
            self.architectures = sorted(list(set(p.Architecture for _, p in self.status.iteritems())))
        else:
            self.status = {}
            self.architectures = None

        self.packages    = None
        self.statusfile  = statusFile
        self.versionfile = versionFile
        self.version = None
        self.basedir = basedir
        self.ipkdir  = ipkdir

    def loadAll(self):
        self.packages = parseManifest(os.path.join(self.ipkdir, 'Packages.gz'))
        if self.architectures:
            for arch in self.architectures:
                p = os.path.join(self.ipkdir, arch, 'Packages.gz')
                self.packages.update(parseManifest(p))
        else:
            archs = []
            for p in glob.glob(os.path.join(self.ipkdir, '*', 'Packages.gz')):
                arch = os.path.basename(os.path.dirname(p))
                if not arch.endswith("-sdk"):
                    archs.append(arch)
                    self.packages.update(parseManifest(p))

            self.architectures = archs

        if self.status:
            outdated = False

            for k, p0 in self.status.iteritems():
                p1 = self.packages[k]
                if not p1:
                    logging.warning("%s: MISSING", k)
                elif p0 == p1:
                    pass
                else:
                    logging.warning("%s: DIFFERENT THAN IMAGE (%s vs %s)",
                                    k, p0.Version, p1.Version)
                    outdated = True

            if outdated:
                logging.warning("TARGET IMAGE OUT OF DATE")
        else:
            logging.warning("NO TARGET IMAGE")

    def loadVersions(self, filename):
        f = open(filename, 'rt')
        v = {}
        for line in f:
            m = versionEx.match(line)
            if m:
                v[m.group(1)] = m.group(2)
        f.close()

        if 'Product release' in v:
            self.versionfile = filename
            self.versions = v
            self.version  = v['Product release']
            return

    def installedPackages(self):
        return sorted(self.status.keys()) if self.status else []

    def Provides(self, name):
        packages = self.packages if self.packages else self.status
        if name in packages:
            return packages[name]

        for k, p in packages.iteritems():
            if k.endswith("-static"):
                pass
            elif p['Provides'] and name in p['Provides']:
                logging.debug("%s: provided by %s", name, p)
                return p

        return None

    def __getitem__(self, k):
        d = self.packages if self.packages else self.status
        return d.get(k)

    def UpdateFrom(self, base):
        packages = []
        wanted = base.installedPackages() if base else []
        needed = self.installedPackages()
        ok = {}
        good = False

        if base:
            logging.info("Generating update from %s (%s)", base.getVersion(), base.statusfile)
        else:
            logging.info("Generating update from any")

        # Packages on the target image
        for k in needed:
            p0 = base[k] if base else None
            p1 = self[k]

            if p0 == None:
                logging.info("%s: NEW (%s)", p1, p1.Version)
                packages.append(p1)
            elif p0 == p1:
                pass
            else:
                logging.info("%s: UPDATED (%s -> %s)",
                        p0, p0.Version, p1.Version)
                packages.append(p1)

            ok[k] = True # don't check again

        # Packages installed on the base
        for k in wanted:
            if k in ok:

                continue
            elif k in self.packages:
                p0 = base[k]
                p1 = self[k]
                if p0 == p1:
                    pass
                else:
                    logging.info("%s: UPDATED (%s -> %s)",
                            p0, p0.Version, p1.Version)
                    logging.debug("%r -> %r", p0, p1)
                    packages.append(p1)

            else:
                logging.warn("%s: GONE", k)

            ok[k] = True # don't check again

        # Dependencies
        while not good:
            good = True
            for p in packages:
                for k in p.Depends:
                    if k in ok:
                        continue

                    p1 = self[k] or self.Provides(k)
                    if not p1:
                        logging.warn("%s: MISSING DEPENDENCY %s", p, k)
                    elif p1.Name in ok:
                        continue
                    else:
                        p0 = base[p1.Name]

                        if p0 == None:
                            logging.info("%s: NEW (%s)", p1, p1.Version)
                            packages.append(p1)
                            good = False
                        elif p0 == p1:
                            pass
                        else:
                            logging.info("%s: UPDATED (%s -> %s)",
                                    p0, p0.Version, p1.Version)
                            packages.append(p1)
                            good = False

                    ok[k] = True # don't check again

        return packages

    def getVersion(self):
        return self.version or "unknown"

    def __iter__(self):
        d = self.status if self.status else self.packages
        return PackageIterator(d)

def do_update(target, *bases):
    target.loadAll()

    for base in bases:
        packages = target.UpdateFrom(base)
        version  = base.getVersion() if base else "any"
        archs    = base.architectures if base else target.architectures

        if len(packages) > 0:
            packages.sort(key = lambda p: "%s/%s" % (p.Architecture, p.Name))
            outdir = os.path.join(target.basedir, "update-from-" + version, 'ipk')
            logging.info("%u packages required into %s/", len(packages), outdir)

            if os.path.exists(outdir):
                shutil.rmtree(outdir)

            for arch in archs:
                os.makedirs(os.path.join(outdir, arch), 0755)

            def copy(f):
                logging.info(" > %s", f)
                f0 = os.path.join(target.ipkdir, f)
                f1 = os.path.join(outdir, f)
                shutil.copy2(f0, f1)

            copy('Packages.gz')
            for arch in archs:
                copy(os.path.join(arch, 'Packages.gz'))

            for p in packages:
                copy(p.Ipkfile)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    manifests = []
    for x in sys.argv[1:]:
        manifests.append(DiscoverManifest(os.path.normpath(x)))

    if len(manifests) > 1:
        do_update(manifests[0], *manifests[1:])
    elif len(manifests) == 1:
        do_update(manifests[0], None)
