# vim: set ft=sh:

mksdimage_ht5() {
	local size="$1"
	local fn= x=

	# construct image name based on the name of the rootfs
	#
	case "$ROOT_IMAGE" in
	*-rootfs.*)
		fn="${ROOT_IMAGE%-rootfs.*}"
		;;
	*)
		fn="${ROOT_IMAGE%.*}"
		;;
	esac

	mksdimage "$size" "$fn.$size.ima" <<-EOT
	,$(part_size_mb 16),c,*
	,$(part_size_mb $ROOT_SIZE_MB),
	,
	EOT

	mkfs_fat16 "$PART1" BOOT
	dump_root_part "$PART2" LINUX
	mkfs_ext4 "$PART3" DATA

	mount_image mnt-root "$PART2" "$PART3"

	cat <<-EOT >> "$ROOT_MOUNT/etc/fstab"
	/dev/mmcblk0p1 /boot vfat defaults,sync,noatime 1 1
	/dev/mmcblk0p3 /usr/local ext4 defaults,noatime 1 1
	EOT

	title "Populating BOOT partition"
	BOOT_MOUNT="$ROOT_MOUNT/mnt"
	mount -t vfat "$PART1" "$BOOT_MOUNT"
	for x in uImage \
		img/u-boot.min.sd:MLO \
		img/u-boot.sd.bin:u-boot.bin \
		; do
		y="${x##*:}"
		x="boot/${x%:*}"

		printf "BOOT:%-16s (%s)\n" "$y" "$x" >&2
		cp -L "$ROOT_MOUNT/$x" "$BOOT_MOUNT/$y"
	done
}
