#!/bin/sh

. "$(dirname "$0")/build_release.inc"

TIS_IMAGE_SIZE=1900

load_base_image() {
	local x=

	if [ -n "${RELEASE_DIR:-}" ]; then
		set -- "$RELEASE_DIR"/*.ima "$RELEASE_DIR"/*.ima.xz
	else
		set -- "$DEPLOY_IMAGE_DIR"/*.ima "$DEPLOY_IMAGE_DIR"/*.ima.xz
	fi

	BASE_IMAGE_FILE=
	for x; do
		if [ -s "$x" ]; then
			BASE_IMAGE_FILE="$x"
			break
		fi
	done

	[ -s "$BASE_IMAGE_FILE" ] || die "base image not found"
}

unload_base_image() {
	BASE_IMAGE_FILE=
	IMAGE_FILE=
}

get_ver_field() {
	sed -ne "s|^$1: \(.*\)$|\1|p;" "$2"
}

mkbundleimage_data() {
	local prefix="$1" version="$2" data="${3:-}"
	local release_name="${prefix##*/}"
	local fn= x=

	case "$data" in
	*/data-*.*)
		fn="${data#*/data}"
		fn="${prefix}${fn%%.*}"
		;;
	*)
		fn="${prefix}-01"
		;;
	esac

	# TODO: use $BASE_IMAGE_FILE's size
	IMAGE_FILE="$fn.$TIS_IMAGE_SIZE.ima"

	# exception catcher
	trap mksdimage_exit_trap EXIT

	info "Creating $IMAGE_FILE"

	case "$BASE_IMAGE_FILE" in
	*.ima.xz)
		xz -dkcv "$BASE_IMAGE_FILE" > "$IMAGE_FILE"
		;;
	*.ima)
		rsync -iSL "$BASE_IMAGE_FILE" "$IMAGE_FILE"
		;;
	*)
		die "$BASE_IMAGE_FILE: unrecognized format"
		;;
	esac

	if [ -n "${SUDO_UID:-}" ]; then
		# make sure the user owns the image
		chown $SUDO_UID${SUDO_GID:+:$SUDO_GID} "$IMAGE_FILE" || true
	fi

	load_card_image
	mount_image mnt-root "$PART2" "$PART3"

	fn="${prefix%/*}/rootfs.tar.gz"
	[ -s "$fn" ] || fn=${prefix}-rootfs.tar.gz

	info "rootfs: $fn"
	rsync -iL "${fn}" "$ROOT_MOUNT/usr/local/HT5/rootfs.tgz"
	if [ -s "$data" ]; then
		info "data..: $data"
		fn="${data##*/}"
		rsync -iL "$data" "$ROOT_MOUNT/usr/local/HT5/data.${fn#*.}"
	fi

	info "swver.: ${release_name}_${version}"
	echo "${release_name}_${version}" > "$ROOT_MOUNT/usr/local/HT5/swver.txt"
}

mkbundleimage() {
	local payloaddir="$1" data=
	local prefix= machine= version= fn=
	local f= has_data=
	local versions=
	shift

	versions=$(ls -1t "$payloaddir"/*-versions.txt "$payloaddir/versions.txt" 2> /dev/null | head -n1)
	if [ "$versions" = "$payloaddir/versions.txt" ]; then
		prefix="$(ls -1t "$payloaddir"/SW-*.txt | head -n1)"
		prefix="${prefix%.txt}"
	elif [ -n "$versions" ]; then
		prefix=${versions%-versions.txt}
	fi

	[ -s "$versions" -o -z "$prefix" ] || die "$payloaddir: invalid release directory"
	machine=$(get_ver_field "Machine" "${versions}")
	version=$(get_ver_field "Product release" "${versions}")

	[ "dm814x-ht5" = "$machine" ] || die "${prefix##*/}: invalid machine ($machine)"

	title "${prefix##*/} $version ($machine)"

	has_data=false
	for f in "${prefix%/*}"/data*.zip "${prefix%/*}"/data*.tar.gz; do
		if [ -s "$f" ]; then
			has_data=true
			mkbundleimage_data "$prefix" "$version" "$f"
		fi
	done
	$has_data || mkbundleimage_data "$prefix" "$version"
	unmount_image

	rsync -iL "$(dirname "$BASE_IMAGE_FILE")/boot.bin" "$payloaddir/"
	chmod 0644 "$payloaddir/boot.bin"

	title "Done"
}

if [ $# -eq 0 ]; then
	load_root_image

	mksdimage_ht5 $TIS_IMAGE_SIZE

	title "Adjusting ROOT partition"

	mkdir "$ROOT_MOUNT/usr/local/HT5"

	title "Done."

	unmount_image
	unload_root_image
else
	load_base_image
	for x; do
		mkbundleimage "$x"
	done
	unload_base_image
fi
