# vim: set ft=sh noet ts=8 sw=8:
set -eu

DEPLOY_SCRIPTS="$(dirname "$0")"

if [ -s "$DEPLOY_SCRIPTS/buildenv.inc" ]; then
	# build's deploy directory

	cd "$DEPLOY_SCRIPTS"
	. ./buildenv.inc
	cd - > /dev/null
	DEPLOY_DIR_IMAGE="$DEPLOY_SCRIPTS/.."
	RELEASE_DIR=
else
	DEPLOY_DIR_IMAGE=
	RELEASE_DIR="$DEPLOY_SCRIPTS/.."
fi

die() {
	if [ $# -eq 0 ]; then
		sed -e 's|^|# FATAL:'
	else
		echo "# FATAL: $*"
	fi >&2
	exit 1
}

info() {
	if [ $# -eq 0 ]; then
		sed -e 's|^|# |'
	else
		echo "# $*"
	fi >&2
}

title() {
	if [ $# -eq 0 ]; then
		echo ""
		echo "#"
		sed -e 's|^|# |'
		echo "#"
	else
		cat <<-EOT

		#
		# $*
		#
		EOT
	fi >&2
}

# SD Image creation
#
part_size_mb() {
	echo $((2048 * ${1:-0}))
}

bytes_to_mb() {
	# rounded up
	dc -e "0k ${1:-0} 1048575 + 1048576 / p"
}

load_root_image() {
	if [ -d "$RELEASE_DIR" ]; then
		ROOT_IMAGE=$(ls -1t "$RELEASE_DIR"/*.ext? | head -n1)
	else
		ROOT_IMAGE=$(ls -1t "$DEPLOY_DIR_IMAGE"/${PRODUCT}-image.${PRODUCT_RELEASE}-${MACHINE}.ext? | head -n1)
	fi
	ROOT_IMAGE_TYPE=$(blkid -o value -s TYPE "$ROOT_IMAGE")
	ROOT_SIZE_BYTES=$(stat -L -c%s "$ROOT_IMAGE")
	ROOT_SIZE_MB=$(bytes_to_mb $ROOT_SIZE_BYTES)
	IMAGE_FILE=
	IMAGE_LOOP=
}

load_card_image() {
	local i= ok= image="${1:-$IMAGE_FILE}"

	IMAGE_LOOP=
	for i in $(seq 1 10); do
		eval "PART$i="
	done
	eval `kpartx -asv "$image" | awk 'BEGIN { count = 0; } /^/ { count++; print "PART" count "=/dev/mapper/" $3; }'`

	ok=false
	for i in $(seq 1 10); do
		eval "p=\$PART$i"
		if [ -b "$p" ]; then
			if [ -z "${IMAGE_LOOP}" ]; then
				p="${p%p*}"
				IMAGE_LOOP="/dev/${p##*/}"
			fi
			ok=true
		else
			eval "PART$i=; unset PART$i"
		fi
	done

	$ok
}

unload_root_image() {
	ROOT_IMAGE=
	IMAGE_FILE=
	IMAGE_LOOP=
}

mkfs_fat16() {
	local dev="$1" label="$2"

	title "Creating $label partition (fat16:$dev)"
	mkfs.vfat -v -F16 -n "$label" "$dev"
}

mkfs_ext3() {
	local dev="$1" label="$2" type="ext3" errno=0
	title "Creating $label partition ($type:$dev)"
	mkfs.$type -F -L "$label" "$dev"

	fsck.$type -af "$dev" || errno=$?
	[ $errno -eq 0 -o $errno -eq 1 ]
}

mkfs_ext4() {
	local dev="$1" label="$2" type="ext4"
	title "Creating $label partition ($type:$dev)"
	mkfs.$type -F -L "$label" "$dev"
	tune2fs -o discard -O ^huge_file "$dev"
	fsck.$type -af "$dev" || errno=$?
	[ $errno -eq 0 -o $errno -eq 1 ]
}

dump_root_part() {
	local dev="$1" label="$2" errno=0
	title <<-EOT
	Creating $label partition ($ROOT_IMAGE_TYPE:$dev)
	using ${ROOT_IMAGE#./}
	EOT
	dd if="$ROOT_IMAGE" of="$dev" bs=1M
	e2label "$dev" "$label"

	fsck.$ROOT_IMAGE_TYPE -af "$dev" || errno=$?
	[ $errno -eq 0 -o $errno -eq 1 ]
}

mksdimage() {
	local i= p= ok=

	IMAGE_SIZE_MB="$1"
	IMAGE_FILE="${2:-$DEPLOY_DIR_IMAGE/${PRODUCT}-image.${PRODUCT_RELEASE}-${MACHINE}.${IMAGE_SIZE_MB}.ima}"
	
	# exception catcher
	trap mksdimage_exit_trap EXIT

	title "Creating $IMAGE_FILE (${IMAGE_SIZE_MB}MiB)"
	rm -f "$IMAGE_FILE"
	dd if=/dev/zero of="$IMAGE_FILE" bs=1M count=0 seek=${IMAGE_SIZE_MB}
	if [ -n "${SUDO_UID:-}" ]; then
		# make sure the user owns the image
		chown $SUDO_UID${SUDO_GID:+:$SUDO_GID} "$IMAGE_FILE" || true
	fi
	sfdisk -uS -L "$IMAGE_FILE"

	load_card_image "$IMAGE_FILE"
}

mount_image() {
	local mnt_root="$1" root_part="$2" data_part="$3"
	local root_part_type=$(blkid -o value -s TYPE "$root_part")
	local data_part_type=$(blkid -o value -s TYPE "$data_part")

	mkdir -p "$mnt_root"
	ROOT_MOUNT="$(cd "$mnt_root" && pwd -P)"

	mount -t "$root_part_type" "$root_part" "$ROOT_MOUNT"

	mkdir -p "$ROOT_MOUNT/usr/local"
	mount -t "$data_part_type" "$data_part" "$ROOT_MOUNT/usr/local"
}

unmount_image() {
	local i= p= busy=

	# unmount partitions
	#
	if [ -e "$IMAGE_FILE" ]; then
		echo -n "# Unmounting $IMAGE_FILE" >&2

		sync
		busy=true
		while $busy; do
			busy=false
			for i in $(seq 1 10); do
				eval "p=\${PART$i:-}"
				if [ -z "$p" ]; then
					:
				elif grep -q "^${p%p*}p" /proc/mounts; then
					busy=true
					grep "^${p%p*}p" /proc/mounts | cut -d' ' -f2 | tac | xargs -r umount || true
					echo -n "."
				else
					if dmsetup remove "$p"; then
						eval PART$i=
					else
						busy=true
					fi
				fi
			done
		done

		if [ -d "${ROOT_MOUNT:-}" ]; then
			while grep -q " $ROOT_MOUNT[ /]" /proc/mounts; do
				echo -n "."
				cut -d' ' -f2 /proc/mounts | grep "^$ROOT_MOUNT" | tac | xargs -r umount || true
			done
			rmdir "$ROOT_MOUNT"
			ROOT_MOUNT=
		fi

		[ -z "$IMAGE_LOOP" ] || losetup -d "$IMAGE_LOOP"

		echo
	fi
}

mksdimage_exit_trap() {
	if [ -s "${IMAGE_FILE:-}" ]; then
		# don't unmount before kpartx was successfully run
		if [ -b "${PART1:-}" ]; then
			unmount_image
		fi
		rm -f "$IMAGE_FILE"
	fi
}

# Release
#
do_release() {
	local ok= x= y= f=
	local basedir="$1"

	RELEASE_NAME="$2"
	RELEASE_DIR="$basedir/$RELEASE_NAME/$PRODUCT_RELEASE"
	shift 2

	title <<-EOT
	Preparing release of $PRODUCT.$PRODUCT_RELEASE.$MACHINE
	$RELEASE_DIR/
	EOT

	rm -rf "$RELEASE_DIR"

	trap release_exit_trap EXIT
	mkdir -p "$RELEASE_DIR"

	# application history
	#
	ok=false
	for x in release.txt release.history; do
		f="$OEBASE/hanover-products/$PRODUCT/$x"
		if [ -s "$f" ]; then
			ok=true
			rsync -iL "$f" "$RELEASE_DIR/$RELEASE_NAME.txt"
			break
		fi
	done
	$ok || die "$PRODUCT: release.txt not found"

	ok=false
	for x in hanover-$PRODUCT-version hanover.version; do
		f="$DEPLOY_DIR_IMAGE/$x"
		if [ -s "$f" ]; then
			ok=true
			rsync -iL "$f" "$RELEASE_DIR/versions.txt"
			break
		fi
	done
	$ok || die "$DEPLOY_DIR_IMAGE: hanover.version not found"

	# rootfs
	#
	rsync -iL "$DEPLOY_DIR_IMAGE/../../ipk/$MACHINE/opkg.status" "$RELEASE_DIR/opkg_status.txt"

	ok=false
	for x in tar.gz ext3 ext4; do
		f="${DEPLOY_DIR_IMAGE}/${PRODUCT}-image.${PRODUCT_RELEASE}-${MACHINE}.$x"
		if [ -s "$f" ]; then
			rsync -iL "$f" "$RELEASE_DIR/$RELEASE_NAME-rootfs.$x"
			ok=true
		fi
	done
	$ok || die "$DEPLOY_DIR_IMAGE: rootfs not found"

	# data
	#
	if [ $# -eq 0 ]; then
		for x in zip tar.gz; do
			for f in "$DEPLOY_DIR_IMAGE"/data.$x "$DEPLOY_DIR_IMAGE"/data-[0-9][0-9].$x; do
				if [ -s "$f" ]; then
					y=${f##*/}
					rsync -iL "$f" "$RELEASE_DIR/$y"
				fi
			done
		done
	else
		for x; do
			ok=false
			for y in $x $x.zip data-$x.zip $x.tar.gz data-$x.tar.gz; do
				f="$DEPLOY_DIR_IMAGE/$y"
				if [ -s "$f" ]; then
					rsync -iL "$f" "$RELEASE_DIR/${f##*/}"
					ok=true
					break
				fi
			done

			$ok || die "data:$x not found."
		done
	fi

	# scripts
	#
	rsync -ai \
		--exclude buildenv.inc \
		--exclude buildenv.sh \
		--exclude release.sh \
		"$DEPLOY_SCRIPTS/" "$RELEASE_DIR/scripts/"

	# ipk
	#
	rsync -ai "$DEPLOY_DIR_IMAGE/../../ipk" "$RELEASE_DIR/"

	# backward compatiblity link
	mkdir -p "$basedir/$PRODUCT.$PRODUCT_RELEASE/$RELDIR"
	ln -sndf ../../$RELEASE_NAME/$PRODUCT_RELEASE "$basedir/$PRODUCT.$PRODUCT_RELEASE/$RELDIR/$MACHINE"

	trap : EXIT
}

do_release_finish() {
	if [ -n "${SUDO_UID:-}" -a -n "$RELEASE_DIR" -a -d "$RELEASE_DIR" ]; then
		# make sure the user owns the image
		chown -R $SUDO_UID${SUDO_GID:+:$SUDO_GID} "$RELEASE_DIR"
	fi
}

release_exit_trap() {
	if [ -n "$RELEASE_DIR" -a -d "$RELEASE_DIR" ]; then
		rm -rf "$RELEASE_DIR"
	fi
}

if [ -z "${MACHINE:-}" ]; then
	for f in "$DEPLOY_SCRIPTS"/release-*.inc; do
		if [ -s "$f" ]; then
			. "$f"
		fi
	done
elif [ -s "$DEPLOY_SCRIPTS/release-$MACHINE.inc" ]; then
	. "$DEPLOY_SCRIPTS/release-$MACHINE.inc"
fi
